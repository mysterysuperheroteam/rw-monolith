def check_params(params, required_params):
    for required_param in required_params:
        if required_param not in params:
            raise Exception("missing " + required_param)
        if params[required_param] is not None:
            try:
                params[required_param] = params[required_param].encode('utf-8mb4')
            except Exception:
                continue
    return

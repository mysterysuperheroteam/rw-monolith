import MySQLdb as db

DB_HOST = "localhost"
DB_USER = "rw"
DB_PASSWORD = "123"
DB_NAME = "monolith"


def connect():
    return db.connect(host=DB_HOST, user=DB_USER, passwd=DB_PASSWORD, db=DB_NAME)


def select_query(con, query, params):
    try:
        cursor = con.cursor()
        cursor.execute(query, params)
        result = cursor.fetchall()
        cursor.close()
    except db.Error as e:
        cursor.close()
    return result

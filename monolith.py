from flask import Flask
from flask import json
from flask import request

import db_connector
import helpers

app = Flask(__name__)


@app.route('/object/get/', methods=['POST'])
def get_object():
    params = request.json
    connection = db_connector.connect()

    try:
        helpers.check_params(params, ["username", "password", "object"])

        username = params["username"]
        password = params["password"]
        object_key = params["object"]

        query = "SELECT username, password FROM users WHERE username = %s AND password = %s;"
        params = (username, password)
        user = db_connector.select_query(connection, query, params)

        if not user:
            return json.dumps({"code": 1, "response": "User not found"})

        query = "SELECT value FROM objects_and_values WHERE object= %s"
        params = (object_key, )
        value = db_connector.select_query(connection, query, params)

        if not value:
            return json.dumps({"code": 1, "response": "Object not found"})

    except Exception as e:
        connection.close()
        return json.dumps({"code": 1, "response": e})

    connection.close()
    return json.dumps({"code": 0, "response": value[0][0]})


if __name__ == '__main__':
    app.run(port=5000)

# Monolith
Данный репозиторий представляет собой пример монолитного приложения, выполняющего функции авторизации и поиска неких данных по ключу в базе данных. 

# Установка
1. Приложение написано с использованием Python-фреймворка Flask 0.12.12. Соответственно, чтобы убедиться, что Flask установлен, следующая команда выведет его версию:
```sh
$ flask --version
```
2. Если Flask не установлен, инструкция по установке расположена [здесь](http://flask.pocoo.org/docs/0.12/installation/).
3. Склонировать репозиторий:
```sh
$ cd path_to_your_folder
$ git clone https://MysterySuperhero@bitbucket.org/mysterysuperheroteam/rw-monolith.git
```

# Внимание!
Для корректной работы приложения необходимо заполнить базу данных в соответствии с шагами, описанными здесь: [Databasefiller](https://bitbucket.org/mysterysuperheroteam/databasefiller)

# Запуск
После заполнения БД необходимо запустить приложение, выполнив следующие команды в терминале (предполагается, что вы находитесь в папке приложения ../monolith):
```sh
$ export FLASK_APP=/path_to_folder/monolith/monolith.py
$ flask run
```
